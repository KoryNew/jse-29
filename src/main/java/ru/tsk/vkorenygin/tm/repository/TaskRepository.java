package ru.tsk.vkorenygin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    protected Predicate<Task> hasName(final String name) {
        return e -> name.equals(e.getName());
    }

    protected Predicate<Task> hasProjectId(final String projectId) {
        return e -> projectId.equals(e.getProjectId());
    }

    @Override
    public @NotNull Optional<Task> findByName(final @NotNull String name, final @NotNull String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .filter(hasName(name))
                .findFirst();
    }

    @Override
    public @NotNull Task changeStatusById(final @NotNull String id, final @NotNull Status status, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findById(id, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) task.ifPresent(e -> e.setStartDate(new Date()));
        return task.get();
    }

    @Override
    public @NotNull Task changeStatusByName(final @NotNull String name, final @NotNull Status status, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findByName(name, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) task.ifPresent(e -> e.setStartDate(new Date()));
        return task.get();
    }

    @Override
    public @NotNull Task changeStatusByIndex(final @NotNull Integer index, final @NotNull Status status, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findByIndex(index, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) task.ifPresent(e -> e.setStartDate(new Date()));
        return task.get();
    }

    @Override
    public @NotNull Task startById(final @NotNull String id, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findById(id, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return task.get();
    }

    @Override
    public @NotNull Task startByIndex(final @NotNull Integer index, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findByIndex(index, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return task.get();
    }

    @Override
    public @NotNull Task startByName(final @NotNull String name, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findByName(name, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return task.get();
    }

    @Override
    public @NotNull Task finishById(final @NotNull String id, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findById(id, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return task.get();
    }

    @Override
    public @NotNull Task finishByIndex(final @NotNull Integer index, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findByIndex(index, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return task.get();
    }

    @Override
    public @NotNull Task finishByName(final @NotNull String name, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findByName(name, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return task.get();
    }

    @Override
    public @NotNull Task bindTaskToProjectById(final @NotNull String projectId, final @NotNull String taskId, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findById(taskId, userId);
        task.ifPresent(e -> e.setProjectId(projectId));
        return task.get();
    }

    @Override
    public @NotNull Task unbindTaskById(final @NotNull String id, final @NotNull String userId) {
        @Nullable final Optional<Task> task = findById(id, userId);
        task.ifPresent(e -> e.setProjectId(null));
        return task.get();
    }

    @Override
    public @NotNull List<Task> findAllByProjectId(final @NotNull String id, final @NotNull String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .filter(hasProjectId(id))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllTaskByProjectId(final @NotNull String id, final @NotNull String userId) {
        entities.stream()
                .filter(hasUser(id))
                .forEach(this::remove);
    }

    @Override
    public @NotNull Optional<Task> removeByName(final @NotNull String name, @NotNull String userId) {
        @Nullable final Optional<Task> task = findByName(name, userId);
        task.ifPresent(e -> entities.remove(e));
        return task;
    }

}
