package ru.tsk.vkorenygin.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractDataCommand;
import ru.tsk.vkorenygin.tm.dto.Domain;
import ru.tsk.vkorenygin.tm.enumerated.Role;

import java.io.FileOutputStream;

public final class DataSaveJsonFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-json-faster";
    }

    @NotNull
    @Override
    public String description() {
        return "Save JSON data to file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SAVE JSON DATA]");
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
