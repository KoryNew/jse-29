package ru.tsk.vkorenygin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.enumerated.Role;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-clear";
    }

    @Override
    public @Nullable String description() {
        return "remove all tasks";
    }

    @Override
    public @NotNull Role @NotNull [] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR TASKS]");
        serviceLocator.getTaskService().clear(currentUserId);
    }

}
