package ru.tsk.vkorenygin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.service.IServiceLocator;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public @NotNull Role[] roles() {
        return new Role[]{};
    }

    public abstract @Nullable String arg();

    public abstract @NotNull String name();

    public abstract @Nullable String description();

    public abstract void execute() throws AbstractException;

}

