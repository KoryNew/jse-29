package ru.tsk.vkorenygin.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.entity.IWBS;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.Date;

public class Project extends AbstractOwnerEntity implements IWBS {

    @Nullable private String name;

    @Nullable private String description;

    @NotNull private Status status = Status.NOT_STARTED;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date startDate;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date createDate = new Date();

    public Project() {
    }

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public @Nullable String getName() {
        return this.name;
    }

    @Override
    public void setName(@NotNull final String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @Override
    @NotNull
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    @Override
    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(@NotNull final Date startDate) {
        this.startDate = startDate;
    }

    @Override
    @NotNull
    public Date getCreateDate() {
        return createDate;
    }

    @Override
    public void setCreateDate(@NotNull final Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public @NotNull String toString() {
        return super.toString() +
                "Name: " + getName() + "; " +
                "Status: " + getStatus() + "; " +
                "Started: " + getStartDate() + "; " +
                "Created: " + getCreateDate() + "; ";
    }

}
